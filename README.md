# Repository has been created for AIDI 2004-01 Lab 1 Evaluation
The goal of this repository is to demonstrate the ability to use version control systems like Git
## Purpose of the Program
The program that has been uploaded to this repository is a simple binary search program that takes an array as a parameter and provides the index of the search element in the array
### Future Work
Next steps include splitting the binary search function and the array test code